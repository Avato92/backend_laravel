<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Players extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
            $table->string('playerName');
            $table->integer('lvl');
            $table->integer('honor');
            $table->integer('gold');
            $table->string('slug');
            $table->integer('str');
            $table->integer('dex');
            $table->integer('int');
            $table->integer('con');
            $table->integer('job');
            $table->string('email');
            $table->string('password');
            $table->rememberToken();
            $table->unsignedInteger('guild_Id');
            $table->timestamps();

            $table->unique('playerName');

            $table->foreign('guild_Id')
            ->references('id')
            ->on('guilds');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
