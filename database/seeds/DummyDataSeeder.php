<?php

use Illuminate\Database\Seeder;

class DummyDataSeeder extends Seeder
{

    protected $totalPlayers = 10;

    protected $totalGuilds = 3;

    /**
     * Populate the database with dummy data for testing.
     * Complete dummy data generation including relationships.
     * Set the property values as required before running database seeder.
     *
     * @param \Faker\Generator $faker
     */
    public function run(\Faker\Generator $faker)
    {

        $guilds = factory(\App\Guild::class)->times($this->totalGuilds)->create();

        $players = factory(\App\Player::class)->times($this->totalPlayers)->create();


        $players->random($faker->numberBetween(1, (int) $players->count()))
        ->each(function ($players) use ($faker, $guilds) {
            $players->guilds()->attach(
                $guilds->random($faker->numberBetween(1, (int) $this->totalGuilds))
            );
        });
    }
}
