<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/



$factory->define(App\Guild::class, function(\Faker\Generator $faker){

    return [
        'guildName' => $faker->unique()->word,
        'lvl' => $faker->numberBetween(1,100),
        'honor' => $faker->numberBetween(1000, 100000),
        'gold' => $faker->numberBetween(1000, 10000),
    ];
});

$factory->define(App\Player::class, function(\Faker\Generator $faker){

    static $guilds;
    $guilds = $guilds ?: \App\Guild::all();
    
    return[
        'playerName' => $faker->unique()->userName,
        'lvl' => $faker->numberBetween(1, 100),
        'honor' => $faker->numberBetween(1000,10000),
        'gold' => $faker->numberBetween(1000,10000),
        'str' => $faker->numberBetween(5,100),
        'dex' => $faker->numberBetween(5,100),
        'int' => $faker->numberBetween(5,100),
        'con' => $faker->numberBetween(5,100),
        'job' => $faker->numberBetween(1,3),
        'guild_Id' => $guilds->random()->id,
        'email' => $faker->word,
        'password' => $faker->word,
    ];
});
