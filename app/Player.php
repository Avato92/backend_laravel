<?php

nameSpace App;

use JWTAuth;
use App\RealWorld\Slug\HasSlug;
use App\RealWorld\Filters\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Player extends Authenticatable{
	use Filterable, HasSlug;

	protected $fillable = [
		'playerName', 'lvl', 'honor', 'gold', 'str', 'dex', 'int', 'con', 'job', 'guild_Id', 'email', 'password'
	];

    protected $hidden = [
        'password'
    ];

    protected $with = [
        'guilds'
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = (password_get_info($value)['algo'] === 0) ? bcrypt($value) : $value;
    }
/**
     * Generate a JWT token for the user.
     *
     * @return string
     */
    public function getTokenAttribute()
    {
        return JWTAuth::fromUser($this);
    }


    public function getGuildListAttribute()
    {
        return Guild::whereId($this['guild_Id'])->pluck("guildName");
    }

    public function scopeLoadRelations($query)
    {
        return $query->orderBy('honor', 'DESC');
    }
    public function guilds()
    {
        return $this->belongsTo(Guild::class);
    }

    public function getSlugSourceColumn()
    {
        return 'playerName';
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}