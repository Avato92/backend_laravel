<?php

namespace App\RealWorld\Transformers;

class PlayerTransformer extends Transformer
{
    protected $resourceName = 'players';

    public function transform($data)
    {
        return [
            'slug'              => $data['slug'],
            'id'             	=> $data['id'],
            'playerName'       	=> $data['playerName'],
            'lvl'              	=> $data['lvl'],
            'honor'           	=> $data['honor'],
            'gold'         		=> $data['gold'],
            'str'               => $data['str'],
            'dex'               => $data['dex'],
            'int'               => $data['int'],
            'con'               => $data['con'],
            'job'               => $data['job'],
            'guild_Id'          => $data['guild_Id'],
            'guildList'         => $data['guildList'],
            'email'             => $data['email'],
            'token'             => $data['token'],
            'password'          => $data['password']
        ];
    }
}