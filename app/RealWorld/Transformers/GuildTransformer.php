<?php

namespace App\RealWorld\Transformers;

class GuildTransformer extends Transformer
{
    protected $resourceName = 'guilds';

    public function transform($data)
    {
        return [
            'slug'              => $data['slug'],
            'id'             	=> $data['id'],
            'guildName'       	=> $data['guildName'],
            'lvl'              	=> $data['lvl'],
            'honor'           	=> $data['honor'],
            'gold'         		=> $data['gold'],
        ];
    }
}