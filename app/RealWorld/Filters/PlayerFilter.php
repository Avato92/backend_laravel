<?php

namespace App\RealWorld\Filters;

use App\Guild;

class PlayerFilter extends Filter
{
    /**
     * Filter by tag name.
     * Get all the articles tagged by the given tag name.
     *
     * @param $name
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function guild($name)
    {
        $guild = Guild::whereid($name)->first();

        $playersIds = $guild ? $guild->players()->pluck('id')->toArray() : [];

        return $this->builder->whereIn('id', $playersIds);
    }
}