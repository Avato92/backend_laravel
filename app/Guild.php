<?php

nameSpace App;

use App\RealWorld\Slug\HasSlug;
use App\RealWorld\Filters\Filterable;
use Illuminate\Database\Eloquent\Model;

class Guild extends Model{
	use Filterable, HasSlug;

	protected $fillable = [
		'guildName', 'lvl', 'honor', 'gold'
	];

    public function getPlayersListAttribute()
    {


        return $this->players->pluck('playerName')->toArray();
    }

        public function scopeLoadRelations($query)
    {
        //return $query->with(['user.followers' => function ($query) {
                //$query->where('follower_id', auth()->id());
            //}])
            //->with(['favorited' => function ($query) {
                //$query->where('user_id', auth()->id());
            //}])
            //->withCount('favorited');
    }

    public function getSlugSourceColumn()
    {
        return 'guildName';
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function players()
    {
        return $this->hasMany(Player::class);
    }
}