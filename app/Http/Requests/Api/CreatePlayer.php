<?php

namespace App\Http\Requests\Api;

class CreatePlayer extends ApiRequest
{
    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        return $this->get('player') ?: [];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
            return [
                'name' => 'required_if:type,==,enroll|max:50|alpha_num|unique:users,username',
                'email' => 'required|email|max:255|unique:users,email',
                'password' => 'required|min:6',
                'class' => 'required_if:type,==,enroll'
            ];
    }
}
