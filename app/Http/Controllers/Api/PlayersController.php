<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Player;
use Illuminate\Http\Request;
use App\Http\Requests\Api\CreatePlayer;
use App\RealWorld\Paginate\Paginate;
use App\RealWorld\Filters\PlayerFilter;
use App\RealWorld\Transformers\PlayerTransformer;

class PlayersController extends ApiController
{

    public function __construct(PlayerTransformer $transformer)
    {
        $this->transformer = $transformer;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PlayerFilter $filter)
    {
       $players = new Paginate(Player::loadRelations()->filter($filter));

        return $this->respondWithPagination($players);

         //$players = Player::all();

        //return $this->respondWithTransformer($players);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePlayer $request)
    {

        $type = $request->input('player.type');

        //REGISTER //
        if($type == "enroll"){
            $player = Player::create([
                'playerName' => $request->input('player.name'),
                'email' => $request->input('player.email'),
                'password' => $request->input('player.password'),
                'job' => $request->input('player.class'),
                'lvl' => 1,
                'honor' => 100,
                'gold' => 1000,
                'str' => 5,
                'dex' => 5,
                'int' => 5,
                'con' => 5,
                'guild_Id' => 1,
            ]);

        return $this->respondWithTransformer($player);
        // LOGIN //
        }else{

            if (Auth::attempt(['email' => $request->input('player.email'), 'password' => $request->input('player.password')])) {
            // Authentication passed...
                return $this->respondWithTransformer(auth()->user());
            }else{
                return $this->respondFailedLogin();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Player $player)
    {
        
        return $this->respondWithTransformer($player);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
