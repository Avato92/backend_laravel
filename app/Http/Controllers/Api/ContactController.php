<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\ContactRequest;
use Mail;

class ContactController extends ApiController
{
    public function sendEmail(ContactRequest $request)
    {
        $reason = $request->input('contact.reason');
        $email = $request->input('contact.email');
        $name = $request->input('contact.name');

        $sent = Mail::raw($reason, function($message)
        {
            $message->subject('Contact Message');
            $message->from('avato92@gmail.com', 'avato92');
            $message->to('avato92@gmail.com');
        });

        if($sent) dd("something wrong"); //var_dump + exit
        
        return response()->json(['message' => 'Request completed']);
    }
}
